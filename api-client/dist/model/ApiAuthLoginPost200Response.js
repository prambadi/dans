"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The ApiAuthLoginPost200Response model module.
 * @module model/ApiAuthLoginPost200Response
 * @version 1.0.0
 */
var ApiAuthLoginPost200Response = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>ApiAuthLoginPost200Response</code>.
   * @alias module:model/ApiAuthLoginPost200Response
   */
  function ApiAuthLoginPost200Response() {
    _classCallCheck(this, ApiAuthLoginPost200Response);

    ApiAuthLoginPost200Response.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(ApiAuthLoginPost200Response, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>ApiAuthLoginPost200Response</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ApiAuthLoginPost200Response} obj Optional instance to populate.
     * @return {module:model/ApiAuthLoginPost200Response} The populated <code>ApiAuthLoginPost200Response</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new ApiAuthLoginPost200Response();

        if (data.hasOwnProperty('data')) {
          obj['data'] = _ApiClient["default"].convertToType(data['data'], Object);
        }

        if (data.hasOwnProperty('message')) {
          obj['message'] = _ApiClient["default"].convertToType(data['message'], 'String');
        }
      }

      return obj;
    }
  }]);

  return ApiAuthLoginPost200Response;
}();
/**
 * @member {Object} data
 */


ApiAuthLoginPost200Response.prototype['data'] = undefined;
/**
 * @member {String} message
 */

ApiAuthLoginPost200Response.prototype['message'] = undefined;
var _default = ApiAuthLoginPost200Response;
exports["default"] = _default;