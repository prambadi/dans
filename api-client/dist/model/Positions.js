"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The Positions model module.
 * @module model/Positions
 * @version 1.0.0
 */
var Positions = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>Positions</code>.
   * @alias module:model/Positions
   */
  function Positions() {
    _classCallCheck(this, Positions);

    Positions.initialize(this);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(Positions, null, [{
    key: "initialize",
    value: function initialize(obj) {}
    /**
     * Constructs a <code>Positions</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/Positions} obj Optional instance to populate.
     * @return {module:model/Positions} The populated <code>Positions</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new Positions();

        if (data.hasOwnProperty('id')) {
          obj['id'] = _ApiClient["default"].convertToType(data['id'], 'String');
        }

        if (data.hasOwnProperty('type')) {
          obj['type'] = _ApiClient["default"].convertToType(data['type'], 'String');
        }

        if (data.hasOwnProperty('url')) {
          obj['url'] = _ApiClient["default"].convertToType(data['url'], 'String');
        }

        if (data.hasOwnProperty('created_at')) {
          obj['created_at'] = _ApiClient["default"].convertToType(data['created_at'], 'String');
        }

        if (data.hasOwnProperty('company')) {
          obj['company'] = _ApiClient["default"].convertToType(data['company'], 'String');
        }

        if (data.hasOwnProperty('company_url')) {
          obj['company_url'] = _ApiClient["default"].convertToType(data['company_url'], 'String');
        }

        if (data.hasOwnProperty('location')) {
          obj['location'] = _ApiClient["default"].convertToType(data['location'], 'String');
        }

        if (data.hasOwnProperty('title')) {
          obj['title'] = _ApiClient["default"].convertToType(data['title'], 'String');
        }

        if (data.hasOwnProperty('description')) {
          obj['description'] = _ApiClient["default"].convertToType(data['description'], 'String');
        }

        if (data.hasOwnProperty('how_to_apply')) {
          obj['how_to_apply'] = _ApiClient["default"].convertToType(data['how_to_apply'], 'String');
        }

        if (data.hasOwnProperty('company_logo')) {
          obj['company_logo'] = _ApiClient["default"].convertToType(data['company_logo'], 'String');
        }
      }

      return obj;
    }
  }]);

  return Positions;
}();
/**
 * 
 * @member {String} id
 */


Positions.prototype['id'] = undefined;
/**
 * 
 * @member {String} type
 */

Positions.prototype['type'] = undefined;
/**
 * 
 * @member {String} url
 */

Positions.prototype['url'] = undefined;
/**
 * 
 * @member {String} created_at
 */

Positions.prototype['created_at'] = undefined;
/**
 * 
 * @member {String} company
 */

Positions.prototype['company'] = undefined;
/**
 * 
 * @member {String} company_url
 */

Positions.prototype['company_url'] = undefined;
/**
 * 
 * @member {String} location
 */

Positions.prototype['location'] = undefined;
/**
 * 
 * @member {String} title
 */

Positions.prototype['title'] = undefined;
/**
 * 
 * @member {String} description
 */

Positions.prototype['description'] = undefined;
/**
 * 
 * @member {String} how_to_apply
 */

Positions.prototype['how_to_apply'] = undefined;
/**
 * 
 * @member {String} company_logo
 */

Positions.prototype['company_logo'] = undefined;
var _default = Positions;
exports["default"] = _default;