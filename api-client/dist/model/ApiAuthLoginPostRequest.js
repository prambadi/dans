"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
 * The ApiAuthLoginPostRequest model module.
 * @module model/ApiAuthLoginPostRequest
 * @version 1.0.0
 */
var ApiAuthLoginPostRequest = /*#__PURE__*/function () {
  /**
   * Constructs a new <code>ApiAuthLoginPostRequest</code>.
   * @alias module:model/ApiAuthLoginPostRequest
   * @param username {String} 
   * @param password {String} 
   */
  function ApiAuthLoginPostRequest(username, password) {
    _classCallCheck(this, ApiAuthLoginPostRequest);

    ApiAuthLoginPostRequest.initialize(this, username, password);
  }
  /**
   * Initializes the fields of this object.
   * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
   * Only for internal use.
   */


  _createClass(ApiAuthLoginPostRequest, null, [{
    key: "initialize",
    value: function initialize(obj, username, password) {
      obj['username'] = username;
      obj['password'] = password;
    }
    /**
     * Constructs a <code>ApiAuthLoginPostRequest</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/ApiAuthLoginPostRequest} obj Optional instance to populate.
     * @return {module:model/ApiAuthLoginPostRequest} The populated <code>ApiAuthLoginPostRequest</code> instance.
     */

  }, {
    key: "constructFromObject",
    value: function constructFromObject(data, obj) {
      if (data) {
        obj = obj || new ApiAuthLoginPostRequest();

        if (data.hasOwnProperty('username')) {
          obj['username'] = _ApiClient["default"].convertToType(data['username'], 'String');
        }

        if (data.hasOwnProperty('password')) {
          obj['password'] = _ApiClient["default"].convertToType(data['password'], 'String');
        }
      }

      return obj;
    }
  }]);

  return ApiAuthLoginPostRequest;
}();
/**
 * @member {String} username
 */


ApiAuthLoginPostRequest.prototype['username'] = undefined;
/**
 * @member {String} password
 */

ApiAuthLoginPostRequest.prototype['password'] = undefined;
var _default = ApiAuthLoginPostRequest;
exports["default"] = _default;