"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _ApiAuthLoginPost200Response = _interopRequireDefault(require("../model/ApiAuthLoginPost200Response"));

var _ApiAuthLoginPostRequest = _interopRequireDefault(require("../model/ApiAuthLoginPostRequest"));

var _Error = _interopRequireDefault(require("../model/Error"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Auth service.
* @module api/AuthApi
* @version 1.0.0
*/
var AuthApi = /*#__PURE__*/function () {
  /**
  * Constructs a new AuthApi. 
  * @alias module:api/AuthApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function AuthApi(apiClient) {
    _classCallCheck(this, AuthApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the apiAuthLoginPost operation.
   * @callback module:api/AuthApi~apiAuthLoginPostCallback
   * @param {String} error Error message, if any.
   * @param {module:model/ApiAuthLoginPost200Response} data The data returned by the service call.
   * @param {String} response The complete HTTP response.
   */

  /**
   * api to login user 
   * @param {module:model/ApiAuthLoginPostRequest} body 
   * @param {module:api/AuthApi~apiAuthLoginPostCallback} callback The callback function, accepting three arguments: error, data, response
   * data is of type: {@link module:model/ApiAuthLoginPost200Response}
   */


  _createClass(AuthApi, [{
    key: "apiAuthLoginPost",
    value: function apiAuthLoginPost(body, callback) {
      var postBody = body; // verify the required parameter 'body' is set

      if (body === undefined || body === null) {
        throw new _Error["default"]("Missing the required parameter 'body' when calling apiAuthLoginPost");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = [];
      var contentTypes = [];
      var accepts = ['*/*'];
      var returnType = _ApiAuthLoginPost200Response["default"];
      return this.apiClient.callApi('/api/auth/login', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the apiAuthRegisterPost operation.
     * @callback module:api/AuthApi~apiAuthRegisterPostCallback
     * @param {String} error Error message, if any.
     * @param {module:model/ApiAuthLoginPost200Response} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * api to register user
     * @param {module:model/ApiAuthLoginPostRequest} body 
     * @param {module:api/AuthApi~apiAuthRegisterPostCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/ApiAuthLoginPost200Response}
     */

  }, {
    key: "apiAuthRegisterPost",
    value: function apiAuthRegisterPost(body, callback) {
      var postBody = body; // verify the required parameter 'body' is set

      if (body === undefined || body === null) {
        throw new _Error["default"]("Missing the required parameter 'body' when calling apiAuthRegisterPost");
      }

      var pathParams = {};
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = [];
      var contentTypes = [];
      var accepts = ['*/*'];
      var returnType = _ApiAuthLoginPost200Response["default"];
      return this.apiClient.callApi('/api/auth/register', 'POST', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return AuthApi;
}();

exports["default"] = AuthApi;