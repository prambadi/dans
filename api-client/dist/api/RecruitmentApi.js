"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ApiClient = _interopRequireDefault(require("../ApiClient"));

var _Error = _interopRequireDefault(require("../model/Error"));

var _Positions = _interopRequireDefault(require("../model/Positions"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

/**
* Recruitment service.
* @module api/RecruitmentApi
* @version 1.0.0
*/
var RecruitmentApi = /*#__PURE__*/function () {
  /**
  * Constructs a new RecruitmentApi. 
  * @alias module:api/RecruitmentApi
  * @class
  * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
  * default to {@link module:ApiClient#instance} if unspecified.
  */
  function RecruitmentApi(apiClient) {
    _classCallCheck(this, RecruitmentApi);

    this.apiClient = apiClient || _ApiClient["default"].instance;
  }
  /**
   * Callback function to receive the result of the apiRecruitmentPositionsIdGet operation.
   * @callback module:api/RecruitmentApi~apiRecruitmentPositionsIdGetCallback
   * @param {String} error Error message, if any.
   * @param data This operation does not return a value.
   * @param {String} response The complete HTTP response.
   */

  /**
   * api to get position job detail
   * @param {String} id 
   * @param {module:api/RecruitmentApi~apiRecruitmentPositionsIdGetCallback} callback The callback function, accepting three arguments: error, data, response
   */


  _createClass(RecruitmentApi, [{
    key: "apiRecruitmentPositionsIdGet",
    value: function apiRecruitmentPositionsIdGet(id, callback) {
      var postBody = null; // verify the required parameter 'id' is set

      if (id === undefined || id === null) {
        throw new _Error["default"]("Missing the required parameter 'id' when calling apiRecruitmentPositionsIdGet");
      }

      var pathParams = {
        'id': id
      };
      var queryParams = {};
      var headerParams = {};
      var formParams = {};
      var authNames = [];
      var contentTypes = [];
      var accepts = ['*/*'];
      var returnType = null;
      return this.apiClient.callApi('/api/recruitment/positions/{id}', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
    /**
     * Callback function to receive the result of the apiRecruitmentPositionsJsonGet operation.
     * @callback module:api/RecruitmentApi~apiRecruitmentPositionsJsonGetCallback
     * @param {String} error Error message, if any.
     * @param {module:model/Positions} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * api to get list position job
     * @param {Object} opts Optional parameters
     * @param {String} opts.page page pagination
     * @param {String} opts.description description filter
     * @param {String} opts.location 
     * @param {Boolean} opts.fullTime 
     * @param {module:api/RecruitmentApi~apiRecruitmentPositionsJsonGetCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/Positions}
     */

  }, {
    key: "apiRecruitmentPositionsJsonGet",
    value: function apiRecruitmentPositionsJsonGet(opts, callback) {
      opts = opts || {};
      var postBody = null;
      var pathParams = {};
      var queryParams = {
        'page': opts['page'],
        'description': opts['description'],
        'location': opts['location'],
        'full_time': opts['fullTime']
      };
      var headerParams = {};
      var formParams = {};
      var authNames = [];
      var contentTypes = [];
      var accepts = ['*/*'];
      var returnType = _Positions["default"];
      return this.apiClient.callApi('/api/recruitment/positions.json', 'GET', pathParams, queryParams, headerParams, formParams, postBody, authNames, contentTypes, accepts, returnType, null, callback);
    }
  }]);

  return RecruitmentApi;
}();

exports["default"] = RecruitmentApi;