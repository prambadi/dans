# JobsApi.RecruitmentApi

All URIs are relative to *http://dev3.dansmultipro.co.id*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiRecruitmentPositionsIdGet**](RecruitmentApi.md#apiRecruitmentPositionsIdGet) | **GET** /api/recruitment/positions/{id} | api to get position job detail
[**apiRecruitmentPositionsJsonGet**](RecruitmentApi.md#apiRecruitmentPositionsJsonGet) | **GET** /api/recruitment/positions.json | api to get list position job



## apiRecruitmentPositionsIdGet

> apiRecruitmentPositionsIdGet(id)

api to get position job detail

### Example

```javascript
import JobsApi from 'jobs_api';

let apiInstance = new JobsApi.RecruitmentApi();
let id = "id_example"; // String | 
apiInstance.apiRecruitmentPositionsIdGet(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## apiRecruitmentPositionsJsonGet

> Positions apiRecruitmentPositionsJsonGet(opts)

api to get list position job

### Example

```javascript
import JobsApi from 'jobs_api';

let apiInstance = new JobsApi.RecruitmentApi();
let opts = {
  'page': "page_example", // String | page pagination
  'description': "description_example", // String | description filter
  'location': "location_example", // String | 
  'fullTime': true // Boolean | 
};
apiInstance.apiRecruitmentPositionsJsonGet(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **String**| page pagination | [optional] 
 **description** | **String**| description filter | [optional] 
 **location** | **String**|  | [optional] 
 **fullTime** | **Boolean**|  | [optional] 

### Return type

[**Positions**](Positions.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

