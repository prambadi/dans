# JobsApi.ApiAuthLoginPost200Response

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | **Object** |  | [optional] 
**message** | **String** |  | [optional] 


