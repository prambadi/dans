# JobsApi.Positions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**url** | **String** |  | [optional] 
**createdAt** | **String** |  | [optional] 
**company** | **String** |  | [optional] 
**companyUrl** | **String** |  | [optional] 
**location** | **String** |  | [optional] 
**title** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**howToApply** | **String** |  | [optional] 
**companyLogo** | **String** |  | [optional] 


