# JobsApi.AuthApi

All URIs are relative to *http://dev3.dansmultipro.co.id*

Method | HTTP request | Description
------------- | ------------- | -------------
[**apiAuthLoginPost**](AuthApi.md#apiAuthLoginPost) | **POST** /api/auth/login | api to login user 
[**apiAuthRegisterPost**](AuthApi.md#apiAuthRegisterPost) | **POST** /api/auth/register | api to register user



## apiAuthLoginPost

> ApiAuthLoginPost200Response apiAuthLoginPost(body)

api to login user 

### Example

```javascript
import JobsApi from 'jobs_api';

let apiInstance = new JobsApi.AuthApi();
let body = new JobsApi.ApiAuthLoginPostRequest(); // ApiAuthLoginPostRequest | 
apiInstance.apiAuthLoginPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiAuthLoginPostRequest**](ApiAuthLoginPostRequest.md)|  | 

### Return type

[**ApiAuthLoginPost200Response**](ApiAuthLoginPost200Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## apiAuthRegisterPost

> ApiAuthLoginPost200Response apiAuthRegisterPost(body)

api to register user

### Example

```javascript
import JobsApi from 'jobs_api';

let apiInstance = new JobsApi.AuthApi();
let body = new JobsApi.ApiAuthLoginPostRequest(); // ApiAuthLoginPostRequest | 
apiInstance.apiAuthRegisterPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ApiAuthLoginPostRequest**](ApiAuthLoginPostRequest.md)|  | 

### Return type

[**ApiAuthLoginPost200Response**](ApiAuthLoginPost200Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

