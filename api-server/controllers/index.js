const AuthController = require('./AuthController');
const RecruitmentController = require('./RecruitmentController');

module.exports = {
  AuthController,
  RecruitmentController,
};
