const AuthService = require('./AuthService');
const RecruitmentService = require('./RecruitmentService');

module.exports = {
  AuthService,
  RecruitmentService,
};
