/* eslint-disable no-async-promise-executor */
/* eslint-disable no-unused-vars */
const apiClient = require('jobs_api');
const Service = require('./Service');

const jobsApi = new apiClient.RecruitmentApi();

/**
* api to get list position job
*
* page String page pagination (optional)
* description String description filter (optional)
* location String  (optional)
* fullUnderscoretime Boolean  (optional)
* returns Positions
* */
const apiRecruitmentPositionsGET = ({
  page, description, location, fullUnderscoretime,
}) => new Promise(
  (resolve, reject) => {
    jobsApi.apiRecruitmentPositionsJsonGet({
      page,
      description,
      location,
      fullUnderscoretime,
    },
    (error, data, response) => {
      if (error) {
        reject(Service.rejectResponse({
          error,
        }, 400));
      } else {
        const result = JSON.parse(response.text);

        resolve(Service.successResponse({
          data: fullUnderscoretime ? result.filter((item) => item.type === 'Full Time') : result,
          message: 'success',
        }));
      }
    });
  },
);

/**
* api to get position job detail
*
* id String
* no response value expected for this operation
* */
const apiRecruitmentPositionsIdGET = ({ id }) => new Promise(
  (resolve, reject) => {
    jobsApi.apiRecruitmentPositionsIdGet(id,
      (error, data, response) => {
        if (error) {
          console.error(error);
          reject(Service.rejectResponse({
            error,
          }, 400));
        } else {
          resolve(Service.successResponse({
            data,
            message: 'success',
          }));
        }
      });
  },
);

module.exports = {
  apiRecruitmentPositionsGET,
  apiRecruitmentPositionsIdGET,
};
