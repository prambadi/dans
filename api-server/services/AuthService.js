/* eslint-disable no-async-promise-executor */
/* eslint-disable no-unused-vars */
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Service = require('./Service');
const { User } = require('../db/models');
const config = require('../config');

/**
 * api to login user
 *
 * body ApiAuthLoginPostRequest
 * returns _api_auth_login_post_200_response
 * */
const apiAuthLoginPOST = ({ body }) => new Promise(async (resolve, reject) => {
  try {
    const user = await User.findOne({ where: { username: body.username } });
    if (!user) {
      reject(Service.rejectResponse({
        message: 'user or password not match',
        code: 401,
      }, 401));
    }

    // console.log(body.p)

    const matchPassword = await bcrypt.compare(body.password, user.password);
    if (!matchPassword) {
      reject(Service.rejectResponse({
        message: 'user or password not match',
        code: 401,
      }, 401));
    }

    const token = jwt.sign({
      id: user.id,
      username: user.username,
    }, config.JWT_SECRET);

    resolve(
      Service.successResponse({
        data: {
          id: user.id,
          username: user.username,
          token,
        },
        message: 'success',
      }),
    );
  } catch (e) {
    reject(
      Service.rejectResponse(e.message || 'Invalid input', e.status || 405),
    );
  }
});
/**
 * api to register user
 *
 * body ApiAuthLoginPostRequest
 * returns _api_auth_login_post_200_response
 * */
const apiAuthRegisterPOST = ({ body }) => new Promise(async (resolve, reject) => {
  try {
    const [user, created] = await User.findOrCreate({
      where: { username: body.username },
      defaults: {
        password: body.password,
      },
    });

    if (!created) {
      reject(Service.rejectResponse({
        message: 'username already taken',
        code: 400,
      }), 200);
    }

    resolve(
      Service.successResponse({
        data: {
          id: user.id,
          user: user.username,
          createdAt: user.createdAt,
          updatedAt: user.updatedAt,
        },
      }),
    );
  } catch (e) {
    reject(
      Service.rejectResponse(e.message || 'Invalid input', e.status || 405),
    );
  }
});

module.exports = {
  apiAuthLoginPOST,
  apiAuthRegisterPOST,
};
