const { User } = require('../models');

module.exports = {
  async up() {
    await User.create({
      username: 'john',
      password: 'doe',
    });
  },

  async down() {
    await User.destroy({ where: { username: 'john' } });
  },
};
