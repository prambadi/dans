import type { ConfigFile } from '@rtk-query/codegen-openapi'

const config: ConfigFile = {
  schemaFile: '../api-server/api/openapi.yaml',
  apiFile: './src/services/emptyApi.ts',
  apiImport: 'emptySplitApi',
  outputFile: './src/services/recruitmentApi.ts',
  exportName: 'recruitmentApi',
  hooks: true,
}

export default config
