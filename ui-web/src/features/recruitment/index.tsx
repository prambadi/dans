import { useEffect, useState } from 'react'

import Button from '../../commons/button'
import Filter from '../../commons/filter'
import List from '../../commons/list'
import { useLazyApiRecruitmentPositionsGetQuery } from '../../services/recruitmentApi'

const Recruitment: React.FC = () => {
  const [job, setJob] = useState('')
  const [location, setLocation] = useState('')
  const [fullTime, setFullTime] = useState(false)
  const [page, setPage] = useState(1)

  const [action, { isLoading, data }] = useLazyApiRecruitmentPositionsGetQuery()

  useEffect(() => {
    const query: any = {}

    if (job) {
      query.description = job
    }

    if (location) {
      query.location = location
    }

    if (page) {
      query.page = page
    }

    action(query)
  }, [job, location, fullTime, page])

  if (isLoading) {
    return <div>...........Loading..........</div>
  }

  return (
    <main className="App">
      <Filter
        job={job}
        setJob={setJob}
        location={location}
        setLocation={setLocation}
        fullTime={fullTime}
        setFullTime={setFullTime}
      />
      {/* {JSON.stringify(data)} */}
      <List data={data} />
      <Button setPage={setPage} page={page} />

      {/* {JSON.stringify(fullTime)} */}
    </main>
  )
}

export default Recruitment
