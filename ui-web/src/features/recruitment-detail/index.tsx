import { useApiRecruitmentPositionsIdGetQuery } from '../../services/recruitmentApi'

const RecruitmentDetail: React.FC = () => {
  const { data, error, isLoading } = useApiRecruitmentPositionsIdGetQuery({
    id: '32bf67e5-4971-47ce-985c-44b6b3860cdb',
  })

  if (isLoading) {
    return <div>...........Loading..........</div>
  }

  return <main className="App">{JSON.stringify(data)}</main>
}

export default RecruitmentDetail
