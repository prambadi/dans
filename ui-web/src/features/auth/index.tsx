import React, { useRef } from 'react'
import { useDispatch } from 'react-redux'
import {
  ApiAuthLoginPostApiArg,
  ApiAuthLoginPostRequest,
  useApiAuthLoginPostMutation,
} from '../../services/recruitmentApi'
import { setCredentials } from './authSlice'

const Auth = () => {
  const [login, result] = useApiAuthLoginPostMutation()
  const dispatch = useDispatch()
  const [formState, setFormState] = React.useState<ApiAuthLoginPostRequest>({
    password: '',
    username: '',
  })

  const handleChange = ({
    target: { name, value },
  }: React.ChangeEvent<HTMLInputElement>) =>
    setFormState((prev) => ({ ...prev, [name]: value }))

  const onSubmit = async (e: any) => {
    e.preventDefault()

    try {
      const data = await login({
        apiAuthLoginPostRequest: {
          username: formState.username,
          password: formState.password,
        },
      }).unwrap()

      // console.log(data.data?)

      dispatch(setCredentials(data?.data))
    } catch (error) {}
  }

  return (
    <div>
      <form onSubmit={onSubmit}>
        <input type="text" onChange={handleChange} name="username"/>
        <input type="password" onChange={handleChange} name="password"/>
        <button type="submit"> Login </button>
      </form>

    </div>
  )
}

export default Auth
