import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import type { RootState } from '../store'
import { Login } from '../../services/recruitmentApi'

export interface LoginRequest {
  username: string
  password: string
}

type AuthState = {
  username: string | undefined
  token: string | undefined
  id: number | undefined
}

const slice = createSlice({
  name: 'auth',
  initialState: {
    username: undefined,
    token: undefined,
    id: undefined,
  } as AuthState,
  reducers: {
    setCredentials: (state, { payload }: PayloadAction<Login | undefined>) => {
      state.username = payload?.username
      state.token = payload?.token
      state.id = payload?.id
    },
  },
})

export const { setCredentials } = slice.actions

export default slice.reducer

export const selectCurrentUser = (state: RootState) => state.auth.id
