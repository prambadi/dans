import cx from 'classnames'
import React, { useState } from 'react'
import { useSelector } from 'react-redux'

import './App.css'
import Detail from './commons/detail'
import Header from './commons/header'
import Auth from './features/auth'
import { selectCurrentUser } from './features/auth/authSlice'
import Recruitment from './features/recruitment'

const App: React.FC = () => {
  const user = useSelector(selectCurrentUser)
  const isDetail = false

  if (!user) {
    return <Auth />
  }

  return (
    <div className={cx('h-screen bg-gray-100')}>
      <Header />
      {!isDetail ? (
        <>
          <Recruitment />
        </>
      ) : (
        <Detail />
      )}
    </div>
  )
}

export default App
