import { emptySplitApi as api } from './emptyApi'
const injectedRtkApi = api.injectEndpoints({
  endpoints: (build) => ({
    apiRecruitmentPositionsGet: build.query<
      ApiRecruitmentPositionsGetApiResponse,
      ApiRecruitmentPositionsGetApiArg
    >({
      query: (queryArg) => ({
        url: `/api/recruitment/positions`,
        params: {
          page: queryArg.page,
          description: queryArg.description,
          location: queryArg.location,
          full_time: queryArg.fullTime,
        },
      }),
    }),
    apiRecruitmentPositionsIdGet: build.query<
      ApiRecruitmentPositionsIdGetApiResponse,
      ApiRecruitmentPositionsIdGetApiArg
    >({
      query: (queryArg) => ({
        url: `/api/recruitment/positions/${queryArg.id}`,
      }),
    }),
    apiAuthLoginPost: build.mutation<
      ApiAuthLoginPostApiResponse,
      ApiAuthLoginPostApiArg
    >({
      query: (queryArg) => ({
        url: `/api/auth/login`,
        method: 'POST',
        body: queryArg.apiAuthLoginPostRequest,
      }),
    }),
    apiAuthRegisterPost: build.mutation<
      ApiAuthRegisterPostApiResponse,
      ApiAuthRegisterPostApiArg
    >({
      query: (queryArg) => ({
        url: `/api/auth/register`,
        method: 'POST',
        body: queryArg.apiAuthLoginPostRequest,
      }),
    }),
  }),
  overrideExisting: false,
})
export { injectedRtkApi as recruitmentApi }
export type ApiRecruitmentPositionsGetApiResponse =
  /** status 200  */ BaseResponse<Positions[]>
export type ApiRecruitmentPositionsGetApiArg = {
  /** page pagination */
  page?: string
  /** description filter */
  description?: string
  location?: string
  fullTime?: boolean
}

export type BaseResponse<T> = {
  data: T
  message: string
}
export type ApiRecruitmentPositionsIdGetApiResponse =
  /** status 200 undefined */ undefined
export type ApiRecruitmentPositionsIdGetApiArg = {
  id: string
}
export type ApiAuthLoginPostApiResponse =
  /** status 200  */ ApiAuthLoginPost200Response
export type ApiAuthLoginPostApiArg = {
  apiAuthLoginPostRequest: ApiAuthLoginPostRequest
}
export type ApiAuthRegisterPostApiResponse =
  /** status 200  */ ApiAuthLoginPost200Response
export type ApiAuthRegisterPostApiArg = {
  apiAuthLoginPostRequest: ApiAuthLoginPostRequest
}
export type Positions = {
  id?: string
  type?: string
  url?: string
  created_at?: string
  company?: string
  company_url?: string
  location?: string
  title?: string
  description?: string
  how_to_apply?: string
  company_logo?: string
}
export type Error = {
  code?: string
  message?: string
}
export type ApiAuthLoginPost200Response = {
  data?: Login
  message?: string
}

export type Login = {
  token: string
  username: string
  id: number
}
export type ApiAuthLoginPostRequest = {
  username: string
  password: string
}
export const {
  useApiRecruitmentPositionsGetQuery,
  useApiRecruitmentPositionsIdGetQuery,
  useApiAuthLoginPostMutation,
  useApiAuthRegisterPostMutation,
  useLazyApiRecruitmentPositionsGetQuery,
  useLazyApiRecruitmentPositionsIdGetQuery,
} = injectedRtkApi
