import cx from 'classnames'
import React from 'react'

const Button: React.FC<{ setPage: (number: number) => void; page: number }> = ({
  setPage,
  page,
}) => {
  return (
    <div className={cx('m-2')}>
      <button
        className={cx(
          'h-auto w-full bg-sky-600 border-b-2 text-center rounded-md text-white font-extrabold text-2xl p-1'
        )}
        onClick={() => setPage(page + 1)}
      >
        {/* <div className={cx('text-2xl font-extrabold text-white p-1')}> */}
        More Job
        {/* </div> */}
      </button>
    </div>
  )
}

export default Button
