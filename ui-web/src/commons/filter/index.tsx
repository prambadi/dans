import cx from 'classnames'
import React from 'react'

const TextInput = ({
  title,
  placeholder,
  onChange,
  value,
}: {
  title: string
  placeholder: string
  onChange: any
  value: string
}) => {
  return (
    <div className={cx('w-2/6')}>
      <div className="block text-sm font-bold text-gray-700">{title}</div>
      <div className="relative mt-1 rounded-md shadow-sm">
        <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
          <svg
            className="h-5 w-5 text-gray-400"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
            fill="currentColor"
            aria-hidden="true"
          >
            <path d="M3 4a2 2 0 00-2 2v1.161l8.441 4.221a1.25 1.25 0 001.118 0L19 7.162V6a2 2 0 00-2-2H3z" />
            <path d="M19 8.839l-7.77 3.885a2.75 2.75 0 01-2.46 0L1 8.839V14a2 2 0 002 2h14a2 2 0 002-2V8.839z" />
          </svg>
        </div>
        <input
          type="email"
          name="email"
          id="email"
          className="block w-full  border-gray-300 pl-10 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
          placeholder={placeholder}
          onChange={onChange}
          value={value}
        />
      </div>
    </div>
  )
}
const Header: React.FC<{
  job: string
  setJob: (text: React.SetStateAction<string>) => void
  location: string
  setLocation: (text: React.SetStateAction<string>) => void
  fullTime: boolean
  setFullTime: (text: React.SetStateAction<boolean>) => void
}> = ({ job, setJob, location, setLocation, setFullTime }) => {
  return (
    <div className={cx('mt-8 flex w-full items-end justify-between p-2')}>
      <TextInput
        title="Job Description"
        placeholder="Filter by title, benefits, companies, expertise"
        onChange={(e: { target: { value: React.SetStateAction<string> } }) =>
          setJob(e.target.value)
        }
        value={job}
      />
      <TextInput
        title="Location"
        placeholder="Filter by City, state, zip code or country"
        onChange={(e: { target: { value: React.SetStateAction<string> } }) =>
          setLocation(e.target.value)
        }
        value={location}
      />
      <div className={cx('flex mb-2')}>
        <input
          type="checkbox"
          className={cx(
            'h-4 w-4 rounded border-gray-300 text-slate-600 focus:ring-slate-500'
          )}
          onChange={
            (e: React.ChangeEvent<HTMLInputElement>) =>
              setFullTime(e.target.checked)
          }
        />
        <div className="ml-2 block text-sm font-bold text-gray-700">
          Full Time Only
        </div>
      </div>
      <button
        type="button"
        className={cx(
          'inline-flex items-center rounded-md border border-transparent bg-slate-400 px-6 py-3 text-base font-medium text-white shadow-sm hover:bg-sky-700 focus:outline-none focus:ring-2 focus:ring-sky-500 focus:ring-offset-2'
        )}
      >
        Search
      </button>
    </div>
  )
}

export default Header
