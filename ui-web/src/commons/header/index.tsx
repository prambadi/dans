import React from 'react'
import cx from 'classnames'
import { useDispatch } from 'react-redux'
import { setCredentials } from '../../features/auth/authSlice'

const Header: React.FC = () => {
  const dispatch = useDispatch()

  return (
    <div
      className={cx(
        'h-auto w-full bg-sky-600 p-2 flex justify-between border-b-2'
      )}
    >
      <div className={cx("flex")}>
        <div className={cx('text-4xl font-extrabold text-white mr-2')}>
          Github
        </div>
        <div className={cx('text-4xl text-white')}>Jobs</div>
      </div>
      <button className="text-right text-white" onClick={() => dispatch(setCredentials())}>
        Logout
      </button>
    </div>
  )
}

export default Header
