import cx from 'classnames'
import React from 'react'

import type { ApiRecruitmentPositionsGetApiResponse } from '../../services/recruitmentApi'

const Card = ({ children }: { children: any }) => {
  return (
    <>
      <div className={cx('m-2 border-4 bg-white p-4')}>{children}</div>
    </>
  )
}

const List: React.FC<{
  data: ApiRecruitmentPositionsGetApiResponse | undefined
}> = ({ data }) => {
  return (
    <div>
      <Card>
        <div className={cx('mb-5 text-4xl font-extrabold text-zinc-800')}>
          Job List
        </div>
        {data?.data?.map((item, id) => (
          <div
            className={cx(
              'flex justify-between border-t-2 border-gray-200 pt-2 pb-2'
            )}
            key={id}
          >
            <div>
              <div className={cx('text-xl font-bold text-sky-700')}>
                {item?.title}
              </div>
              <div className={cx('flex items-center justify-center')}>
                <div>{item?.company}</div>
                <div className={cx('mx-2')}>-</div>
                <div className={cx('text-sm font-extrabold text-lime-600')}>
                  {item?.type}
                </div>
              </div>
            </div>
            <div>
              <div className={cx('text-sm font-extrabold text-gray-600')}>
                {item?.location}
              </div>
              <div className={cx('text-sm font-normal text-gray-400')}>
                {item?.created_at}
              </div>
            </div>
          </div>
        ))}
      </Card>
    </div>
  )
}

export default List
